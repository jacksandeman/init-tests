describe('Console Error Test', () => {
    it('Checks to see if there are any errors in the console', () => {
        cy.visit('/', {
            onBeforeLoad(win) {
                cy.spy(win.console, 'error');
            }
        });

        cy.window().then((win) => {
            expect(win.console.error).to.have.callCount(0);
        });
    })
});

describe('Page Load Test', () => {
    it('Checks that the page returns 200 (success)', () => {
        cy
            .request('/')
            .then(resp => {
                expect(resp.status).to.eq(200)
            })
    })
});

// Useful for if error being thrown and want to test anyways
// cy.on('uncaught:exception', () => {
//     cy.log('There is error pls fix');
// });

describe('Admin Login Test', () => {
    it('Check that the admin page loads correctly', () => {
        cy.visit('/wp-admin');

        cy.contains('Username or Email Address').debug();
        cy.contains('Password');

        cy.log('working');
        cy
            .get('#user_login')
            .type('admin');

        cy
            .get('#user_pass')
            .type('admin');

        cy
            .get('#wp-submit')
            .click()
    })
});

describe('Cookie Bar Appears', () => {
    it('Checks that a cookie bar appears on first load but not after accepted', () => {
        cy.visit('/');

        cy.contains(Cypress.env('cookieMessage'));
        cy
            .get(Cypress.env('cookieButtonSelector'))
            .click({ force: true });
        cy.wait(100);

        cy.reload();
        cy.contains('This website uses cookies').should('not.exist')
    })
});
