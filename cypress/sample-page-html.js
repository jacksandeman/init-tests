export const samplePageHTML = `<!-- wp:image {"id":59} -->
<figure class="wp-block-image"><img src="https://wpdotorg.files.wordpress.com/2008/11/boat.jpg" alt="Boat" class="wp-image-59"/><figcaption>Boat</figcaption></figure>
<!-- /wp:image -->

<!-- wp:separator -->
<hr class="wp-block-separator"/>
<!-- /wp:separator -->

<!-- wp:heading {"level":1} -->
<h1>Heading 1</h1>
<!-- /wp:heading -->

<!-- wp:heading -->
<h2>Heading 2</h2>
<!-- /wp:heading -->

<!-- wp:heading {"level":3} -->
<h3>Heading 3</h3>
<!-- /wp:heading -->

<!-- wp:heading {"level":4} -->
<h4>Heading 4</h4>
<!-- /wp:heading -->

<!-- wp:heading {"level":5} -->
<h5>Heading 5</h5>
<!-- /wp:heading -->

<!-- wp:heading {"level":6} -->
<h6>HEADING 6</h6>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p><a href="https://wp-themes.com/twentytwenty/#wrapper">[top]</a></p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator"/>
<!-- /wp:separator -->

<!-- wp:heading -->
<h2 id="paragraph">Paragraph</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Lorem ipsum dolor sit amet,&nbsp;<a href="https://wp-themes.com/twentytwenty/#">test link</a>&nbsp;adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Lorem ipsum dolor sit amet,&nbsp;<em>emphasis</em>&nbsp;consectetuer adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://wp-themes.com/twentytwenty/#wrapper">[top]</a></p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator"/>
<!-- /wp:separator -->

<!-- wp:heading -->
<h2 id="list_types">List Types</h2>
<!-- /wp:heading -->

<!-- wp:heading {"level":3} -->
<h3>Ordered List</h3>
<!-- /wp:heading -->

<!-- wp:list {"ordered":true} -->
<ol><li>List Item 1</li><li>List Item 2</li><li>List Item 3</li></ol>
<!-- /wp:list -->

<!-- wp:heading {"level":3} -->
<h3>Unordered List</h3>
<!-- /wp:heading -->

<!-- wp:list -->
<ul><li>List Item 1</li><li>List Item 2</li><li>List Item 3</li></ul>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><a href="https://wp-themes.com/twentytwenty/#wrapper">[top]</a></p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator"/>
<!-- /wp:separator -->

<!-- wp:heading -->
<h2 id="tables">Tables</h2>
<!-- /wp:heading -->

<!-- wp:table -->
<figure class="wp-block-table"><table class=""><tbody><tr><th>Table Header 1</th><th>Table Header 2</th><th>Table Header 3</th></tr><tr><td>Division 1</td><td>Division 2</td><td>Division 3</td></tr><tr><td>Division 1</td><td>Division 2</td><td>Division 3</td></tr><tr><td>Division 1</td><td>Division 2</td><td>Division 3</td></tr></tbody></table></figure>
<!-- /wp:table -->

<!-- wp:paragraph -->
<p><a href="https://wp-themes.com/twentytwenty/#wrapper">[top]</a></p>
<!-- /wp:paragraph -->

<!-- wp:separator -->
<hr class="wp-block-separator"/>
<!-- /wp:separator -->

<!-- wp:heading -->
<h2 id="misc">Misc Stuff – abbr, acronym, pre, code, sub, sup, etc.</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Lorem&nbsp;<sup>superscript</sup>&nbsp;dolor&nbsp;<sub>subscript</sub>&nbsp;amet, consectetuer adipiscing elit. Nullam dignissim convallis est. Quisque aliquam.&nbsp;cite. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy.&nbsp;NBA&nbsp;Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.&nbsp;<abbr title="Avenue">AVE</abbr></p>
<!-- /wp:paragraph -->

<!-- wp:preformatted -->
<pre class="wp-block-preformatted">Lorem ipsum dolor sit amet,
 consectetuer adipiscing elit.
 Nullam dignissim convallis est.
 Quisque aliquam. Donec faucibus. 
Nunc iaculis suscipit dui. 
Nam sit amet sem. 
Aliquam libero nisi, imperdiet at,
 tincidunt nec, gravida vehicula,
 nisl. 
Praesent mattis, massa quis 
luctus fermentum, turpis mi 
volutpat justo, eu volutpat 
enim diam eget metus. 
Maecenas ornare tortor. 
Donec sed tellus eget sapien
 fringilla nonummy. 
NBA 
Mauris a ante. Suspendisse
 quam sem, consequat at, 
commodo vitae, feugiat in, 
nunc. Morbi imperdiet augue
 quis tellus.  
<abbr title="Avenue">AVE</abbr></pre>
<!-- /wp:preformatted -->

<!-- wp:quote -->
<blockquote class="wp-block-quote"><p>“This stylesheet is going to help so freaking much.”<br>-Blockquote</p></blockquote>
<!-- /wp:quote -->

<!-- wp:paragraph -->
<p><a href="https://wp-themes.com/twentytwenty/#wrapper">[top]</a></p>
<!-- /wp:paragraph -->`;